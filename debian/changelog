pass-tomb (1.3-4) unstable; urgency=medium

  * Move aliased files to /usr (Closes: #1073759).
  * Updating copyright for debian directory.

 -- David Kunz <david.kunz@dknet.ch>  Thu, 04 Jul 2024 15:17:02 +0200

pass-tomb (1.3-3) unstable; urgency=medium

  * Fix file locations. (Closes: #1034240).

 -- David Kunz <david.kunz@dknet.ch>  Tue, 25 Apr 2023 10:41:49 +0200

pass-tomb (1.3-2) unstable; urgency=medium

  * Change path from /lib to /usr (Closes: #1003996).

 -- David Kunz <david.kunz@dknet.ch>  Wed, 19 Jan 2022 07:49:30 +0100

pass-tomb (1.3-1) unstable; urgency=medium

  * Merging upstream version 1.3.
  * Adding watch file.
  * Updating copyright.
  * Updating Standards-Version to 4.6.0.
  * Updating vcs in control.

 -- David Kunz <david.kunz@dknet.ch>  Mon, 10 Jan 2022 14:04:36 +0100

pass-tomb (1.2-1) experimental; urgency=medium

  * Releasing debian version 1.2-1 (Closes: #951119).
  * Fixing install location bash-completion.
  * Updating debhelper.
  * Updating standards-version.
  * Adding Copyrights for new version.
  * Enabling dh_auto_test.

 -- David Kunz <david.kunz@dknet.ch>  Fri, 19 Feb 2021 12:53:08 +0100

pass-tomb (1.1-4.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 16:22:09 +0100

pass-tomb (1.1-4) unstable; urgency=medium

  * Remove build depends because the testsuite was already deactivated
    (Closes: #916885).

 -- David Kunz <david.kunz@dknet.ch>  Fri, 11 Jan 2019 13:27:49 +0100

pass-tomb (1.1-3) unstable; urgency=medium

  * Updating year in copyright for 2019.
  * Updating to standards version 4.3.0.
  * Updating to debhelper 12.
  * Change email address
  * Change from debhelper to debhelper-compat

 -- David Kunz <david.kunz@dknet.ch>  Fri, 04 Jan 2019 10:15:54 +0100

pass-tomb (1.1-2) unstable; urgency=medium

  * Change Vcs-Git in control.
  * Add Rules-Requires-Root.
  * Add Enhances.

 -- David Kunz <david.kunz@dknet.ch>  Mon, 10 Sep 2018 13:26:10 +0200

pass-tomb (1.1-1) unstable; urgency=medium

  * Initial release (Closes: #907633).

 -- David Kunz <david.kunz@dknet.ch>  Thu, 30 Aug 2018 13:27:30 +0200
